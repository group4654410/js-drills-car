function sortedCarNames(inventory) {
  let car_models = []

  if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0) {
    return;
  }

  for (let index = 0; index < inventory.length; index++) {
    car_models.push(inventory[index].car_model);
  }

  car_models.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));

  for (let index = 0; index < car_models.length; index++) {
    console.log(car_models[index]);
  }
  return car_models;
}

module.exports = sortedCarNames;
