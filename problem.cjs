const inventory = require("./data.cjs");

const problem1 = require("./problem1.cjs");
const problem2 = require("./problem2.cjs");
const problem3 = require("./problem3.cjs");
const problem4 = require("./problem4.cjs");
const problem5 = require("./problem5.cjs");
const problem6 = require("./problem6.cjs");

// Solution 1:
problem1(inventory, 33);

// Solution 2:
problem2(inventory);

// Solution 3:
problem3(inventory);

// Solution 4:
let years = problem4(inventory);

// Solution 5:
problem5(inventory, years, 2000);

// Solution 6:
let models = problem6(inventory, ["Audi", "BMW"]);
