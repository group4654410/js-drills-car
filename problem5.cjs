function olderCarsYears(inventory, carYears, limit) {
    
    let oldCarsData = [];

    if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0 || carYears === undefined || Array.isArray(carYears) === false || carYears.length === 0 || limit === undefined || Number.isNaN(limit)) {
        return;
    }

    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_year < limit){
            oldCarsData.push(inventory[index]);
        }
    }
    console.log(oldCarsData.length);
    return oldCarsData;
}

module.exports = olderCarsYears;
