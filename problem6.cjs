function selctModels(inventory, carModels) {
    
    let selectCarData = [];

    if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0 || carModels === undefined || Array.isArray(carModels) === false || carModels.length === 0) {
        return;
    }

    for (let index = 0; index < inventory.length; index++) {
        if (carModels.includes(inventory[index].car_make)){
            selectCarData.push(inventory[index]);
        }
    }

    for (let index = 0; index < selectCarData.length; index++) {
        console.log(JSON.stringify(selectCarData[index], null, "    "));
    }

    return selectCarData;
}

module.exports = selctModels;
