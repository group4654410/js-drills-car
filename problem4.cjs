function allCarYears(inventory) {

    let car_years = [];

    if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0) {
        return;
    }

    for (let index = 0; index < inventory.length; index++) {
        car_years.push(inventory[index].car_year);
        console.log(inventory[index].car_year);
    }
    return car_years;
}

module.exports = allCarYears;
