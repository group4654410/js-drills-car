function lastCarDetails(inventory) {
  if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0) {
    return;
  }

  let lastCar = inventory[inventory.length - 1];

  console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
  return lastCar;
}

module.exports = lastCarDetails;
