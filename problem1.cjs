function recallCarDetails(inventory, car_id) {

  if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0 || car_id === undefined || Number.isNaN(car_id) === true) {
    return [];
  }

  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].id === car_id) {
      console.log(`Car 33 is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`);
      return inventory[index];
    }
  }
  return [];
}

module.exports = recallCarDetails;
